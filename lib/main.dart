import 'package:flutter/material.dart';
import 'package:gojek_ui/features/clone_jek/presenter/constant/colors.dart';
import 'package:gojek_ui/features/clone_jek/presenter/pages/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: GoogleFonts.robotoTextTheme(
          Theme.of(context)
              .textTheme, // If this is not set, then ThemeData.light().textTheme is used.
        ),
        //primaryColor: CloneJekColor.darkGreen,
      ),
      home: HomePage(),
    );
  }
}
