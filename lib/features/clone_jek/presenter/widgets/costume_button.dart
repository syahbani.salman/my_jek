import 'package:flutter/material.dart';
import 'package:gojek_ui/features/clone_jek/presenter/constant/colors.dart';
import 'package:gojek_ui/features/clone_jek/presenter/constant/font_size.dart';

class CostumeButton extends StatelessWidget {
  CostumeButton({Key? key, required this.icName, required this.text})
      : super(key: key);

  final String icName;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 6),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/$icName",
            height: 25,
            width: 25,
            fit: BoxFit.cover,
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              text,
              style: TextStyle(
                  color: CloneJekColor.white,
                  fontSize: MyFontSize.medium1,
                  fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
