import 'package:flutter/material.dart';
import 'package:gojek_ui/features/clone_jek/presenter/constant/colors.dart';
import 'package:gojek_ui/features/clone_jek/presenter/constant/font_size.dart';
import 'package:gojek_ui/features/clone_jek/presenter/widgets/costume_button.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<bool> tabBarBadgeList = [false, false, false, true];
  final tabBarList = ["Beranda", "Promo", "Pesanan", "Chat"];
  int tabBarIndex = 0;

  Widget searchBox() {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            height: 50,
            decoration: BoxDecoration(
                color: CloneJekColor.white2,
                borderRadius: BorderRadius.circular(100),
                border: Border.all(
                  color: CloneJekColor.lightGray,
                  width: 1.5,
                )),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  color: CloneJekColor.black,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: Text(
                  "Cari layanan, makanan, & tujuan",
                  style: TextStyle(
                      color: CloneJekColor.gray, fontSize: MyFontSize.medium2),
                )),
              ],
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(1000),
          child: Image.network(
            "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870",
            width: 60,
            height: 60,
            fit: BoxFit.cover,
          ),
        )
      ],
    );
  }

  Widget tabBarItem(int index) {
    return Expanded(
      child: Stack(children: [
        Container(
          margin: EdgeInsets.all(3),
          height: double.infinity,
          decoration: BoxDecoration(
              color: (tabBarIndex == index)
                  ? CloneJekColor.white
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(100)),
          child: InkWell(
            onTap: () {
              setState(() {
                tabBarIndex = index;
              });
            },
            child: Center(
              child: Text(
                tabBarList[index],
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: (tabBarIndex == index)
                        ? CloneJekColor.darkGreen
                        : CloneJekColor.white,
                    fontSize: MyFontSize.medium1),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        if (tabBarBadgeList[index])
          Align(
            alignment: Alignment.topRight,
            child: Container(
              height: 20,
              width: 20,
              decoration: BoxDecoration(
                  color: CloneJekColor.red,
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(
                    color: CloneJekColor.white,
                    width: 1.5,
                  )),
              child: Center(
                child: Container(
                  height: 4,
                  width: 4,
                  decoration: BoxDecoration(
                    color: CloneJekColor.white,
                    borderRadius: BorderRadius.circular(100),
                  ),
                ),
              ),
            ),
          )
      ]),
    );
  }

  Widget appBar() {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
          color: CloneJekColor.darkGreen,
          borderRadius: BorderRadius.circular(100)),
      child: Row(
        children: [
          tabBarItem(0),
          tabBarItem(1),
          tabBarItem(2),
          tabBarItem(3),
        ],
      ),
    );
  }

  Widget balance() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.all(10),
      width: double.infinity,
      height: 130,
      decoration: BoxDecoration(
          color: CloneJekColor.darkBlue,
          borderRadius: BorderRadius.circular(20)),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 3,
                margin: EdgeInsets.symmetric(vertical: 5),
                height: 13,
                decoration: BoxDecoration(color: CloneJekColor.lightGray),
              ),
              Container(
                width: 3,
                margin: EdgeInsets.symmetric(vertical: 5),
                height: 13,
                decoration: BoxDecoration(color: CloneJekColor.white),
              ),
            ],
          ),
          Container(
            height: double.infinity,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: CloneJekColor.white,
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "gopay",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: MyFontSize.medium2),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 6),
                  child: Text(
                    "Saldo masih kosong",
                    style: TextStyle(color: CloneJekColor.brown),
                  ),
                ),
                Text(
                  "Klik buat isi",
                  style: TextStyle(
                      color: CloneJekColor.green, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          CostumeButton(icName: "ic_bayar.png", text: "Bayar"),
          CostumeButton(icName: "ic_topup.png", text: "Top Up"),
          CostumeButton(icName: "ic_eksplor.png", text: "Eksplor"),
        ],
      ),
    );
  }

  Widget goClub() {
    return Container(
      padding: EdgeInsets.all(20),
      width: double.infinity,
      decoration: BoxDecoration(
        color: CloneJekColor.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  "assets/images/ic_indoclub.png",
                  width: 30,
                  height: 30,
                ),
                Text(
                  "goclub",
                  style: TextStyle(fontSize: MyFontSize.large2),
                ),
              ],
            ),
            Text(
              "program loyalitasnya Gojek",
              style: TextStyle(fontWeight: FontWeight.w400),
            ),
          ],
        ),
        SizedBox(
          width: 40,
        ),
        InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            decoration: BoxDecoration(
                color: CloneJekColor.green,
                borderRadius: BorderRadius.circular(40)),
            child: Text(
              "Ikutan gratis",
              style: TextStyle(color: CloneJekColor.white),
            ),
          ),
        )
      ]),
    );
  }

  Widget gopayLater() {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          " gopaylater",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: MyFontSize.medium2),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Pake GoPayLater di Tokopedia",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: MyFontSize.medium2,
            ),
          ),
        ),
        Text(
          "Belanja & nikmati beragam cashback istimewa hingga Rp. 1,7 juta untuk-mu~",
          style: TextStyle(fontSize: MyFontSize.medium2),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              height: 200,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                image: DecorationImage(
                    image: AssetImage("assets/images/gojek_promo.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Hadiah dari Gopay buat perjalanmu!",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: MyFontSize.medium2),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.symmetric(vertical: 5),
              child: Text(
                "Nikmatin perjalanan aman dan hemat naik GoRide/GoCar. Bayar pake gopay diskon sampai Rp. 76,000 pakai kode GOTOMERDEKA. Klik!",
                style: TextStyle(fontSize: MyFontSize.medium1),
              ),
            )
          ],
        )
      ],
    ));
  }

  Widget floatingNavbar() {
    return Container(
      height: 100,
      margin: EdgeInsets.only(left: 30),
      decoration: BoxDecoration(
          color: CloneJekColor.white, borderRadius: BorderRadius.circular(50)),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/ic_indoride.png",
                    width: 40,
                    height: 40,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text("GoRide"),
                  )
                ],
              ),
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/ic_indocar.png",
                    width: 40,
                    height: 40,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text("GoCar"),
                  )
                ],
              ),
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/ic_indofood.png",
                    width: 40,
                    height: 40,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text("GoFood"),
                  )
                ],
              ),
            ),
            InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/ic_indosend.png",
                    width: 40,
                    height: 40,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text("GoSend"),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CloneJekColor.green,
        title: appBar(),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          searchBox(),
          balance(),
          goClub(),
          gopayLater(),
          floatingNavbar()
        ],
      ),
      floatingActionButton: floatingNavbar(),
    );
  }
}
