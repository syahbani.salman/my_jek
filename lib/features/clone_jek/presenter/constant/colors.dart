import 'package:flutter/material.dart';

class CloneJekColor {
  static const Color black = Color(0xff1C1E1D);
  static const Color brown = Color(0xff494B4A);
  static const Color white = Color(0xffFFFFFF);
  static const Color white2 = Color(0xffFAFAFA);
  static const Color green = Color(0xff00880C);
  static const Color darkGreen = Color(0xff077310);
  static const Color red = Color(0xffE9001E);
  static const Color darkBlue = Color(0xff0081A0);
  static const Color lightGray = Color(0xffE8E8E8);
  static const Color gray = Color(0xffBBBBBB);
  static const Color softGreen = Color(0xffE9FFEA);
}
